<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="halaman/workstyle1.css">
    <title>Work</title>
</head>
<body>
    
    <div class="header">
        <h1>WORK</h1>
        <p>Beberapa Karya yang sudah saya buat.</p>
    </div>

    <div class="container">
        <a href="halaman/work/design.php">
            <div class="box">
            <h2>Design</h2>
            </div>
        </a>
        <a href="halaman/work/photo.php">
            <div class="box">
            <h2>Photography</h2>
            </div>
        </a>

    </div>

</body>
</html>