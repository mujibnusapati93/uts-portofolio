<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="style1.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Design</title>
</head>
<body>

    <a href="../../index.php?page=work">
    <div class="back">
        <img src="bahandesign/back2.png" alt="">
    </div>
    </a>

    <div class="header">
        <h1>Design</h1>
        <p>Beberapa Karya yang telah saya buat.</p>
    </div>

    
    

<div class="bg">
        <div class="container">
        <div class="card">
            <div class="head-card">
            <img src="bahandesign/1.png" alt="">
            </div>
            <div class="body-card">
            <h1>Kopi Foska</h1>
            <p>Kopi froska ini dijual di daerah tempat tinggal saya hampir setiap hari saya membelinya, kopi ini memiliki rasa yang sangat enak dan harga yang terjangkau namun pengunjungnya masih sedikit. Ketika saya melihat postingan instragram kedai ini masih hanya foto produk biasa tanpa editing, mulai dari situ saya berinisiatif untuk mengedit foto produk ini menjadi lebih menarik dengan tambahan background yang kontras dan balance </p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
        <div class="head-card">
            <img src="bahandesign/2.png" alt="">
        </div>
        <div class="body-card">
            <h1>Promosi Rental Usaha</h1>
            <p>Memasuki usia kepala dua, seorang teman mencoba memulai usaha barunya dengan menyewakan kendaraam mobil namun karena belum memiliki relasi seluas orang yang sudah berpengalaman ia meminta bantuan kepada saya untuk membuatkan poster promosi dari usahanya tersebut.
            </p>
            <h4>24 July 2021</h4>
        </div>
        </div>	

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/3.png" alt="">
            </div>
            <div class="body-card">
            <h1>Sepatu Air jordan</h1>
            <p>Desain ini dibuat karena permintaan dari teman dekat saya mendesain sebuah produk sepatu untuk memenuhi tugas kuliahnya.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/4.png" alt="">
            </div>
            <div class="body-card">
            <h1>Home</h1>
            <p>Desain ini juga dibuat karena permintaan seorang teman, desain rumah simpe ini tidak dibuat dengan aplikasi 3D namun harus terlihat 3D.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/5.png" alt="">
            </div>
            <div class="body-card">
            <h1>Crypto</h1>
            <p>Desain inidibuat untuk memenuhi tugas kuliah saya pada saat semester 3, awalnya ini adalah sebuat coretan gambar menggunakan pensil yang diberikan oleh seorang dosen dan didesain dengan menggunakan corel draw dengan pemilihan warna secara pribadi tidak ditentukan oleh dosen.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/6.png" alt="">
            </div>
            <div class="body-card">
		  <h1>Molly</h1>
		  <p>Molly dibuat untuk desain produk saya dengan tujuan untuk mengangkat tema keragaman indonesia, maka dari itu terlihat molly disini menggunakan topi berbentuk cula badak. Dimana badak bercula satu ini disebut juga sebagai badak jawa indonesia yang banyak hidup di pulau jawa dan pulau sumatera.</p>
          <h4>24 July 2021</h4>
        </div>
	    </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/7.png" alt="">
            </div>
            <div class="body-card">
            <h1>Adventure</h1>
            <p>Desain ini dibuat untuk desain produk saya, dipilih seperti ini karena sedang mendapatkan ispirasi tentang advanture.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/8.png" alt="">
            </div>
            <div class="body-card">
            <h1>Beras Kencur</h1>
            <p>Desain ini dibuat untuk mengisi feed instagram carijamu, mempromosikan produk mitra dari carijamu yaitu dari mustikaratu.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/9.png" alt="">
            </div>
            <div class="body-card">
            <h1>Cuci Tangan</h1>
            <p>Desain ini dibuat untuk mengisi feed instagram carijamu untuk memperingati hari-hari besar salah satunya yaitu hari cuci tangan sedunia.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/10.png" alt="">
            </div>
            <div class="body-card">
            <h1>Sumpah Pemuda</h1>
            <p>Desain ini dibuat untuk mengisi feed instagram carijamu untuk memperingati hari-hari besar salah satunya yaitu hari sumpah pemuda.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/11.png" alt="">
            </div>
            <div class="body-card">
            <h1>Halimah Bakery</h1>
            <p>Saudara sepupu saya baru membuka usaha snack dan minta bantuan untuk membuatkan logo namun karena kurangnya pengalaman dan inspirasi maka dari itu logo ini jadi tidak sinkron dan tidak terpakai.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/12.png" alt="">
            </div>
            <div class="body-card">
            <h1>Hiring</h1>
            <p>Poster ini dibuat untuk open requirments karyawan baru di MUSEE VISUAL.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/13.png" alt="">
            </div>
            <div class="body-card">
            <h1>Nadifd</h1>
            <p>Karena saya bekerja sebagai desain grafik di flash saya melihat peluang bisnis dengan menawarkan jasa membuat logo kepada orang tua riders di event pushbike , logo ini mengandung unsur sailor moon.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/14.png" alt="">
            </div>
            <div class="body-card">
            <h1>Sibanas</h1>
            <p>Logo ini dibuat karena permintaan dari pacar saya yang mengikuti karya tulis ilmiah dengan membuat produk berbahan alam untuk meningkatkan imunitas tubuh di masa pandemi covid-19 yaitu sirup madu bawang dayak dan nanas.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/15.png" alt="">
            </div>
            <div class="body-card">
            <h1>IMaBs</h1>
            <p>Logo ini dibuat  karena permintaan seorang teman untuk jurusan International Program of Management And Busines Universitas Muhammadiyah Yogyakarta.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
            <img src="bahandesign/16.png" alt="">
            </div>
            <div class="body-card">
            <h1>Cutecar</h1>
            <p>Logo ini dibuat oleh permintaan seorang klien untuk usaha mainan anak - anak yang berada di Yogyakarta.</p>
            <h4>24 July 2021</h4>
            </div>
        </div>
    </div>
</div>

    <div class="footer">
    <p>Jika kalian tertarik untuk membuat sebuah 
        Design, mau itu logo, poster atau apalah, 
        bisa kita obrolin di Whatzupp.</p>
    </div>

</body>
</html>