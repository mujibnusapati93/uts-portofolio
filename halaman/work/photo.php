<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylephoto.css">
    <title>Document</title>
</head>
<body>

<a href="../../index.php?page=work">
    <div class="back">
        <img src="bahandesign/back2.png" alt="">
    </div>
    </a>

    <div class="header">
        <h1>Photography</h1>
    </div>

    

    <div class="container">
        
        <div class="card">
            <div class="head-card">
                <h3>Bluee</h3>
            </div>
            <div class="body-card">
                <img src="photo/1.png" alt="">
            </div>
            <div class="footer-card">
                <h4>18 September 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Birtday Party</h3>
            </div>
            <div class="body-card">
                <img src="photo/2.png" alt="">
            </div>
            <div class="footer-card">
                <h4>21 September 2020</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>White</h3>
            </div>
            <div class="body-card">
                <img src="photo/3.png" alt="">
            </div>
            <div class="footer-card">
                <h4>18 September 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Alone</h3>
            </div>
            <div class="body-card">
                <img src="photo/4a.png" alt="">
            </div>
            <div class="footer-card">
                <h4>10 September 2020</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Volk</h3>
            </div>
            <div class="body-card">
                <img src="photo/5.png" alt="">
            </div>
            <div class="footer-card">
                <h4>11 September 2019</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Sunrise</h3>
            </div>
            <div class="body-card">
                <img src="photo/6.png" alt="">
            </div>
            <div class="footer-card">
                <h4>24 February 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Stand</h3>
            </div>
            <div class="body-card">
                <img src="photo/7.png" alt="">
            </div>
            <div class="footer-card">
                <h4>27 June 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Kapal</h3>
            </div>
            <div class="body-card">
                <img src="photo/8.png" alt="">
            </div>
            <div class="footer-card">
                <h4>05 October 2020</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Prewedding</h3>
            </div>
            <div class="body-card">
                <img src="photo/9.png" alt="">
            </div>
            <div class="footer-card">
                <h4>10 January 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Wedding</h3>
            </div>
            <div class="body-card">
                <img src="photo/10.png" alt="">
            </div>
            <div class="footer-card">
                <h4>11 November 2020</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Ocean</h3>
            </div>
            <div class="body-card">
                <img src="photo/11a.png" alt="">
            </div>
            <div class="footer-card">
                <h4>23 September 2021</h4>
            </div>
        </div>

        <div class="card">
            <div class="head-card">
                <h3>Graduation #2</h3>
            </div>
            <div class="body-card">
                <img src="photo/12.png" alt="">
            </div>
            <div class="footer-card">
                <h4>28 October 2021</h4>
            </div>
        </div>

    </div>
    
</body>
</html>